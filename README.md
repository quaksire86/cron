## Cron parser project

### Valid inputs
Input
* 30 1 /bin/run_me_daily 16:10
* 45 * /bin/run_me_hourly 16:10
* * * /bin/run_me_every_minute 16:10
* * 19 /bin/run_me_sixty_times 16:10

### Expected outputs
* 1:30 tomorrow - /bin/run_me_daily
* 16:45 today - /bin/run_me_hourly
* 16:10 today - /bin/run_me_every_minute
* 19:00 today - /bin/run_me_sixty_times


### How to run the project
You can run the project using the next gradle command
```aidl
 ./gradlew run --args="* * /command 16:10"             
```

### To improve
* Handle input from files
* Improve documentation about how to pass arguments as input

### Notes
Please note that no external library has been used for this project, only MockK to help on testing
