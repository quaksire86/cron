package cron

import parser.date.CronDate
import parser.date.CronDateParser
import parser.line.Cron
import parser.line.CronLineParser
import util.TimeRegex.HOUR_REGEX
import util.TimeRegex.MINUTE_REGEX
import util.TimeUtils
import util.TimeUtilsImpl

// We can use this constructor to inject mock parsers on unit test
class CronInstructor internal constructor(
    private val builder: Builder,
    private val timeUtils: TimeUtils,
    private val instructionLineParser: CronLineParser,
    private val dateLineParser: CronDateParser
) : CronInstruction {

    private constructor(
        builder: Builder
    ): this(builder, TimeUtilsImpl(), CronLineParser(), CronDateParser())


    override fun execute() : String {
        val cronLine = builder.instructionLine
        val startingTime = builder.dateLine

        if (cronLine.isNullOrEmpty()) {
            throw IllegalArgumentException("Cron instruction empty or null")
        }
        if (startingTime.isNullOrEmpty()) {
            throw IllegalArgumentException("Starting Time empty or null")
        }

        val cronCommand = instructionLineParser.parse(cronLine)
        val cronDate = dateLineParser.parse(startingTime)

        val hourToDisplay = getDisplayHour(cronCommand, cronDate)
        val minuteToDisplay = getDisplayMinute(cronCommand, cronDate)
        val dayToDisplay = if (timeUtils.isToday(cronDate.hour, cronDate.minute)) { "today" } else { "tomorrow" }

        return "$hourToDisplay:$minuteToDisplay $dayToDisplay - ${cronCommand.command}"
    }

    private fun getDisplayHour(cronCommand: Cron, cronDate: CronDate) = if (cronCommand.hour == "*") {
        cronDate.hour
    } else if (HOUR_REGEX.toRegex().matches(cronCommand.hour)) {
        cronCommand.hour
    } else {
        throw CronInvalidHourException()
    }

    private fun getDisplayMinute(cronCommand: Cron, cronDate: CronDate) = if (cronCommand.minute == "*") {
        cronDate.minute
    } else if (MINUTE_REGEX.toRegex().matches(cronCommand.minute)) {
        cronCommand.minute
    } else {
        throw CronInvalidMinuteException()
    }

    class Builder {
        var instructionLine: String? = null
            private set
        var dateLine: String? = null
            private set

        fun instruction(instruction: String) = apply {
            this.instructionLine = instruction
        }

        fun date(date: String) = apply {
            this.dateLine = date
        }

        fun build() = CronInstructor(this)
    }
}

