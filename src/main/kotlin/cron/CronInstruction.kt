package cron

interface CronInstruction {
    fun execute(): String
}