import cron.CronInstructor

class CronParser {
    companion object {
        /**
         *
         * Input
         * 30 1 /bin/run_me_daily
         * 45 * /bin/run_me_hourly
         * * * /bin/run_me_every_minute
         * * 19 /bin/run_me_sixty_times
         *
         * Output
         * 1:30 tomorrow - /bin/run_me_daily
         * 16:45 today - /bin/run_me_hourly
         * 16:10 today - /bin/run_me_every_minute
         * 19:00 today - /bin/run_me_sixty_times
         *
         */
        @JvmStatic fun main(args: Array<String>) {
            args.forEach { println(it) }
            if (args.isEmpty()) {
                println("Arguments are empty, please provide a valid CRON expression and starting time. E.g.:\n\"30 * /bin/test\" 16:40")
            } else if ((args.size % 3) == 1) {
                val date = args.last()
                var cron = args.joinToString { it }
                cron = cron.substring(0, cron.length - date.length).replace(",", "").trimEnd()

                val instruction = CronInstructor
                    .Builder()
                    .instruction(cron)
                    .date(date)
                    .build()
                    .execute()
                println(instruction)
            } else if (args.size != 2) {
                println("Too many arguments, please review them. arguments passed:")
                args.map { argumentLine -> println(argumentLine) }
            } else {
                val instruction = CronInstructor
                    .Builder()
                    .instruction(args[0])
                    .date(args[1])
                    .build()
                    .execute()
                println(instruction)
            }
        }
    }
}

