package parser

interface CronParser<T> {
    fun parse(cronLine: String): T
}