package parser.date

data class CronDate(
    val hour: Int,
    val minute: Int
)