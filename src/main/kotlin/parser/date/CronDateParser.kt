package parser.date

import parser.CronParser
import util.TimeRegex.HOUR_REGEX
import util.TimeRegex.MINUTE_REGEX

class CronDateParser: CronParser<CronDate> {

    override fun parse(cronLine: String): CronDate {
        if (isLineValid(cronLine)) {
            val parsed = cronLine.split(":")
            val hour = parsed[0].trim()
            val minute = parsed[1].trim()
            // Check if hour and minute are valid Int
            if (!HOUR_REGEX.toRegex().matches(hour) || hour.isEmpty()) {
                throw CronDateNotValidRegexException("Hour not valid: $hour")
            }
            if (!MINUTE_REGEX.toRegex().matches(minute) || minute.isEmpty()) {
                throw CronDateNotValidRegexException("Minute not valid: $minute")
            }

            return CronDate(
                hour = hour.toInt(),
                minute = minute.toInt()
            )
        } else {
            throw CronDateLineNotValidSizeException()
        }
    }

    private fun isLineValid(cronLineDate: String) : Boolean {
        return cronLineDate.contains(":") && cronLineDate.split(":").size == 2
    }
}