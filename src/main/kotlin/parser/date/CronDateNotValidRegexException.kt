package parser.date

import java.lang.RuntimeException

class CronDateNotValidRegexException(
    private val cronDateExceptionMessage: String
): RuntimeException(cronDateExceptionMessage)