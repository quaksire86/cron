package parser.date

import java.lang.RuntimeException

class CronDateLineNotValidSizeException: RuntimeException()