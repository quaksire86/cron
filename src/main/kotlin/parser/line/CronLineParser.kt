package parser.line

import parser.CronParser

class CronLineParser: CronParser<Cron> {
    override fun parse(cronLine: String): Cron {
        if (isLineValid(cronLine)) {
            val cronLineParsed = cronLine.split(" ")
            return Cron(
                hour = cronLineParsed[1],
                minute = cronLineParsed[0],
                command = cronLineParsed[2]
            )
        } else {
            throw CronLineNotValidSizeException()
        }
    }

    private fun isLineValid(cronLine: String): Boolean {
        return cronLine.contains(" ") && cronLine.split(" ").size == 3
    }
}