package parser.line

import java.lang.RuntimeException

class CronLineNotValidSizeException: RuntimeException()