package parser.line

data class Cron(
    val hour: String,
    val minute: String,
    val command: String
)