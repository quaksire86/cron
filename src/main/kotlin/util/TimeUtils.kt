package util

import java.time.LocalTime

interface TimeUtils {
    fun isToday(hour: Int, minute: Int) : Boolean
}

class TimeUtilsImpl: TimeUtils {
    override fun isToday(hour: Int, minute: Int) : Boolean {
        return LocalTime.now().isBefore(LocalTime.of(hour, minute))
    }
}