package util

object TimeRegex {
    const val HOUR_REGEX = "^([0-1]?[0-9]|2[0-3])"
    const val MINUTE_REGEX = "^([0-5][0-9])"
}