package parser.line

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CronLineParserTest {

    private val parser = CronLineParser()

    @Test
    fun `given valid line, then the correct Cron object is returned`() {
        val result = parser.parse("45 * /bin/run_me_hourly")

        assert(result.command == "/bin/run_me_hourly")
        assert(result.hour == "*")
        assert(result.minute == "45")
    }

    @Test
    fun `given empty line, then exception is thrown`() {
        Assertions.assertThrows(CronLineNotValidSizeException::class.java) { parser.parse("") }
    }

    @Test
    fun `given missing instruction, then exception is thrown`() {
        Assertions.assertThrows(CronLineNotValidSizeException::class.java) { parser.parse("45 *") }
    }

    @Test
    fun `given more arguments, then exception is thrown`() {
        Assertions.assertThrows(CronLineNotValidSizeException::class.java) { parser.parse("45 * /bin/run_me_hourly other") }
    }

}