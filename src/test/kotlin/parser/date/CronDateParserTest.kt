package parser.date

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CronDateParserTest {

    private val parser = CronDateParser()

    @Test
    fun `given valid time, then correct date parsed returned`() {
        val result = parser.parse("16:10")
        assert(result.hour == 16)
        assert(result.minute == 10)
    }

    @Test
    fun `given valid time, when hour has leading 0, then correct date parsed returned`() {
        val result = parser.parse("06:10")
        assert(result.hour == 6)
        assert(result.minute == 10)
    }

    @Test
    fun `given valid time, when hour has not leading 0, then correct date parsed returned`() {
        val result = parser.parse("6:10")
        assert(result.hour == 6)
        assert(result.minute == 10)
    }

    @Test
    fun `given empty time, then exception is thrown`() {
        Assertions.assertThrows(CronDateLineNotValidSizeException::class.java) { parser.parse("") }
    }

    @Test
    fun `given empty hour, then exception is thrown`() {
        Assertions.assertThrows(CronDateNotValidRegexException::class.java) { parser.parse(":10") }
    }

    @Test
    fun `given empty minute, then exception is thrown`() {
        Assertions.assertThrows(CronDateNotValidRegexException::class.java) { parser.parse("16:") }
    }

    @Test
    fun `given alphabetical hour, then exception is thrown`() {
        Assertions.assertThrows(CronDateNotValidRegexException::class.java) { parser.parse("AB:10") }
    }

    @Test
    fun `given alphabetical minute, then exception is thrown`() {
        Assertions.assertThrows(CronDateNotValidRegexException::class.java) { parser.parse("16:CD") }
    }

    @Test
    fun `given out of range hour, then exception is thrown`() {
        Assertions.assertThrows(CronDateNotValidRegexException::class.java) { parser.parse("56:10") }
    }

    @Test
    fun `given out of range minute, then exception is thrown`() {
        Assertions.assertThrows(CronDateNotValidRegexException::class.java) { parser.parse("16:78") }
    }
}