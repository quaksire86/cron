package cron

import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import parser.date.CronDateParser
import parser.line.CronLineParser
import util.TimeUtils

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CronInstructionTest {

    @MockK
    private lateinit var timeUtils: TimeUtils

    @BeforeAll
    fun beforeAll() {
        MockKAnnotations.init(this, relaxed = true)
    }

    @Test
    fun `given valid today scenario, then valid instruction is returned`() {
        every {
            timeUtils.isToday(15, 30)
        } returns true

        val builder = CronInstructor
            .Builder()
            .instruction("10 16 /command")
            .date("15:30")
        val result = CronInstructor(builder, timeUtils, CronLineParser(), CronDateParser()).execute()
        assert(result == "16:10 today - /command")
    }

    @Test
    fun `given valid tomorrow scenario, then valid instruction is returned`() {
        every {
            timeUtils.isToday(18, 30)
        } returns false

        val builder = CronInstructor
            .Builder()
            .instruction("10 16 /command")
            .date("18:30")
        val result = CronInstructor(builder, timeUtils, CronLineParser(), CronDateParser()).execute()
        assert(result == "16:10 tomorrow - /command")
    }

    @Test
    fun `given valid today scenario, when minute is star, then valid instruction is returned`() {
        every {
            timeUtils.isToday(15, 30)
        } returns true

        val builder = CronInstructor
            .Builder()
            .instruction("* 16 /command")
            .date("15:30")
        val result = CronInstructor(builder, timeUtils, CronLineParser(), CronDateParser()).execute()
        assert(result == "16:30 today - /command")
    }

    @Test
    fun `given valid today scenario, when hour is star, then valid instruction is returned`() {
        every {
            timeUtils.isToday(15, 30)
        } returns true

        val builder = CronInstructor
            .Builder()
            .instruction("45 * /command")
            .date("15:30")
        val result = CronInstructor(builder, timeUtils, CronLineParser(), CronDateParser()).execute()
        assert(result == "15:45 today - /command")
    }

    @Test
    fun `given null command, then throw exception`() {
        every {
            timeUtils.isToday(15, 30)
        } returns true

        val builder = CronInstructor
            .Builder()
            .date("15:30")
        val instruction = CronInstructor(builder, timeUtils, CronLineParser(), CronDateParser())

        Assertions.assertThrows(IllegalArgumentException::class.java) { instruction.execute() }
    }

    @Test
    fun `given empty command, then throw exception`() {
        every {
            timeUtils.isToday(15, 30)
        } returns true

        val builder = CronInstructor
            .Builder()
            .instruction("")
            .date("15:30")
        val instruction = CronInstructor(builder, timeUtils, CronLineParser(), CronDateParser())

        Assertions.assertThrows(IllegalArgumentException::class.java) { instruction.execute() }
    }

    @Test
    fun `given null date, then throw exception`() {
        every {
            timeUtils.isToday(15, 30)
        } returns true

        val builder = CronInstructor
            .Builder()
            .instruction("45 * /command")
        val instruction = CronInstructor(builder, timeUtils, CronLineParser(), CronDateParser())

        Assertions.assertThrows(IllegalArgumentException::class.java) { instruction.execute() }
    }

    @Test
    fun `given empty date, then throw exception`() {
        every {
            timeUtils.isToday(15, 30)
        } returns true

        val builder = CronInstructor
            .Builder()
            .instruction("45 * /command")
            .date("")
        val instruction = CronInstructor(builder, timeUtils, CronLineParser(), CronDateParser())

        Assertions.assertThrows(IllegalArgumentException::class.java) { instruction.execute() }
    }

    @Test
    fun `given unsupported hour command, then throw exception`() {
        every {
            timeUtils.isToday(15, 30)
        } returns true

        val builder = CronInstructor
            .Builder()
            .instruction("45 ? /command")
            .date("15:30")
        val instruction = CronInstructor(builder, timeUtils, CronLineParser(), CronDateParser())

        Assertions.assertThrows(CronInvalidHourException::class.java) { instruction.execute() }
    }

    @Test
    fun `given unsupported minute command, then throw exception`() {
        every {
            timeUtils.isToday(15, 30)
        } returns true

        val builder = CronInstructor
            .Builder()
            .instruction("? 12 /command")
            .date("15:30")
        val instruction = CronInstructor(builder, timeUtils, CronLineParser(), CronDateParser())

        Assertions.assertThrows(CronInvalidMinuteException::class.java) { instruction.execute() }
    }
}